class Author < ActiveRecord::Base
  attr_accessible :name, :picture, :asset_ids
  validates :name, :presence => true, :uniqueness => true
  has_and_belongs_to_many :assets
end
