class Show < ActiveRecord::Base
  attr_accessible :description, :name, :asset_id
  validates_presence_of :name, :description

  has_many :assets
end
