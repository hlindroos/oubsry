class AssetsAuthors < ActiveRecord::Base
    belongs_to :asset
    belongs_to :author
end
