class Asset < ActiveRecord::Base
    attr_accessible :checksum, :description, :duration, :file, :filesize, :visability, :height, :path, :release, :show_id, :state, :title, :dlcount, :aspect, :media, :width, :author_ids, :category_id, :agelimit, :agelimitreason
    belongs_to :show
    belongs_to :category
    has_and_belongs_to_many :authors
    has_and_belongs_to_many :tags

    mount_uploader :file, AssetUploader

    before_save :store_metadata #fill in record metadata
    after_save  :enqueue #render webfiles

    validates_presence_of :agelimitreason, :if => :agelimit_set?
    validates_presence_of :title, :description, :release, :show, :category, :authors
    validates_associated :tags

    def self.agelimits
        {"N/A" => 0, "K7" => 7,"K11" => 11,"K13" => 13,"K15" => 15,"K18" => 18}
    end

    def self.visability_modes
        {"All" => 0, "No Internet" => 1}
    end

    VIDEO = 1
    IMAGE = 2
    AUDIO = 3

    def self.is_video?
        self.media == VIDEO
    end

    def self.is_image?
        self.media == IMAGE
    end

    def self.is_audio?
        self.media == AUDIO
    end

    def agelimit_set?
        agelimit != 0
    end

    state_machine :initial => :pending do

        event :screenshot do
            transition :pending => :screenshotmade
        end

        event :convert do
            transition :screenshotmade => :converting
        end

        event :converted do
            transition :converting => :converted
        end

        event :convertfail do
            transition :converting => :error
        end

    end

    def tag!(tags)
        newtags = tags.split(", ").map do |tag|
            Tag.find_or_create_by_name(tag)
        end

        remove = self.tags.select {|r| newtags.exclude? r}
        add    = newtags.select {|a| self.tags.exclude? a}
        add.uniq!
        remove.each do |r|
            self.tags.delete(r)
        end

        self.tags << add
    end

    #  # Returns filename camelized. Replace non-alphanumeric, underscore and periods with "camelization"
    #def sanitize_filename(value)
    #  # get only the filename, not the whole path
    #  just_filename = value.gsub(/^.*(\\|\/)/, '')
    #  # NOTE: File.basename doesn't work right with Windows paths on Unix
    #  # INCORRECT: just_filename = File.basename(value.gsub('\\\\', '/')) 

    #  # Finally, replace all non alphanumeric, underscore or periods with underscore in the filename and camilizes it
    #  just_filename.gsub(/[^\w\.\-]/,'_').camelize
    #end

    private
    def store_metadata
        if self.file.present? && self.changed.include?("file") 
            file_path = "#{Rails.root}/public/#{file}"
            file = Mediainfo.new file_path
            common_meta(file, file_path)
            if file.image?
                image_meta(file)
                self.media = IMAGE
            elsif file.video?
                video_meta(file)
                self.media = VIDEO
            elsif file.audio?
                audio_meta(file)
                self.media = AUDIO
            end
        end
    end

    def common_meta(file, file_path)
        self.filesize = file.size
        self.checksum = ::Digest::MD5.file(file_path).hexdigest
        self.container = file.format
        self.path = File.dirname(file_path)
    end

    def video_meta(file)
        self.duration = file.video.duration
        self.width = file.video.width
        self.height = file.video.height
        self.bitrate = file.overall_bit_rate
        self.framerate = file.video.frame_rate
        self.aspect = file.video.display_aspect_ratio
        self.vcodec = "#{file.video.format_info} / #{file.video.format_profile} / #{file.video.codec_id}"
        self.interlaced = 1 if file.video.interlaced?
        if file.audio.count == 0
            self.samplerate  = 0
            self.acodec = "NOAUDIO"
            self.channels = "NOAUDIO"
        else
            self.samplerate = file.audio.parsed_response[:audio]['sampling_rate']
            self.acodec = file.audio.parsed_response[:audio]['format_profile']
            self.channels = file.audio.parsed_response[:audio]['channel_s']
        end
    end

    def image_meta(file)
        self.width = file.image.width
        self.height = file.image.height
        self.bitrate = file.image[0].parsed_response[:image]['bit_depth']
        self.vcodec = file.image[0].parsed_response[:image]['format_compression']
        self.aspect = self.width/self.height
    end

    def audio_meta(file)
        self.duration = file.duration
        self.bitrate = file.overall_bit_rate
        self.samplerate = file.audio.parsed_response[:audio]['sampling_rate']
        self.acodec = file.audio.parsed_response[:audio]['format_profile']
        self.channels = file.audio.parsed_response[:audio]['channel_s']
    end


end
