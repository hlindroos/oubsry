# encoding: utf-8

class AssetUploader < CarrierWave::Uploader::Base
    include CarrierWave::FFMPEG
    include CarrierWave::Delayed::Job 

    # Include RMagick or MiniMagick support:
    # include CarrierWave::RMagick
    # include CarrierWave::MiniMagick

    # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
    # include Sprockets::Helpers::RailsHelper
    # include Sprockets::Helpers::IsolatedHelper

    # Choose what kind of storage to use for this uploader:
    storage :file
    # storage :fog

    # Override the directory where uploaded files will be stored.
    # This is a sensible default for uploaders that are meant to be mounted:
    def store_dir
        #"uploads/#{self.show.name}/#{self.created_at.year.to_s}/"
        #"uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
        "#{Rails.root}/uploads"
        #"#{Rails.root}/uploads/#{self.show.name}"
    end

    # Provide a default URL as a default if there hasn't been a file uploaded:
    # def default_url
    #   # For Rails 3.1+ asset pipeline compatibility:
    #   # asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
    #
    #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
    # end

    # Process files as they are uploaded:
    # process :scale => [200, 300]
    #
    def scale(width, height)
      # do something
    end

    # Create different versions of your uploaded files:
    # version :thumb do
    #   process :scale => [50, 50]
    # end

    # Add a white list of extensions which are allowed to be uploaded.
    # For images you might use something like this:
    def extension_white_list
        %w(jpg jpeg gif png avi mp4 m2p mpg wav mp3)
    end

    # Override the filename of the uploaded files:
    # Avoid using model.id or version_name here, see uploader/store.rb for details.
    #def filename
    #  "something.jpg" if original_filename
    #end

    # before :store, :remember_cache_id
    # after :store, :delete_tmp_dir

    # # store! nil's the cache_id after it finishes so we need to remember it for deletion
    # def remember_cache_id(new_file)
    #     @cache_id_was = cache_id
    # end

    # def delete_tmp_dir(new_file)
    #     # make sure we don't delete other things accidentally by checking the name pattern
    #     if @cache_id_was.present? && @cache_id_was =~ /\A[\d]{8}\-[\d]{4}\-[\d]+\-[\d]{4}\z/
    #         FileUtils.rm_rf(File.join(cache_dir, @cache_id_was))
    #     end
    # end

    # version :image do, :if => :is_image?
    #     version :thumb
    # end

    # version :audio do, :if => :is_audio?
    #     version :mp3
    # end

    # version :video do, :if => :is_video?
    #     version :thumb
    #     version :webfiles
    # end

    version :ss, :if => :is_video? do
        process :make_jpg
        def full_filename(for_file)
            "#{File.basename(for_file, File.extname(for_file))}.jpg"
        end
    end

    version :mp4, :if => :is_video? do
        process :make_mp4
        def full_filename(for_file)
            "#{File.basename(for_file, File.extname(for_file))}.mp4"
        end
    end

    version :webm, :if => :is_video? do
        process :make_webm
        def full_filename(for_file)
            "#{File.basename(for_file, File.extname(for_file))}.webm"
        end
    end

    version :mp3, :if => :is_audio? do
        process :make_mp3
        def full_filename(for_file)
            "#{File.basename(for_file, File.extname(for_file))}.mp3"
        end
    end

    protected
    def is_video? asset
        model.media == 1
    end

    def is_audio? asset
        model.media == 2
    end

    def is_image? asset
        model.media == 3
    end

end
