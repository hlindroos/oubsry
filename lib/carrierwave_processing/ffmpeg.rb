require 'streamio-ffmpeg'
module CarrierWave
    module FFMPEG
        extend ActiveSupport::Concern
        module ClassMethods
            def make_jpg
                process :make_jpg
            end
            def make_mp4
                process :make_mp4
            end
            def make_webm
                process :make_webm
            end
            def make_mp3
                process :make_mp3
            end
        end

        def make_jpg
            # move upload to local cache
            cache_stored_file! if !cached?
            
            directory = File.dirname( current_path )
            tmp_path   = File.join( directory, "tmpfile" )
            puts "directory: " + directory
            puts "tmp_path: " + tmp_path
            puts "current: " + current_path
            File.rename current_path, tmp_path

            file = ::FFMPEG::Movie.new( tmp_path )
            file.transcode( current_path, "-ss 1  -vframes 1 -r 1 -s 640x360 -f image2")
            #file.transcode( webfile, "-ss 5  -vframes 1 -r 1 -s 640x360 -f image2")
            #file.transcode( webfile, "-ss 10 -vframes 1 -r 1 -s 640x360 -f image2")
            fixed_name = File.basename(current_path, '.*') + ".jpg"
            File.rename File.join( directory, fixed_name ), current_path
            File.delete tmp_path
        end

        def make_mp4
            # move upload to local cache
            cache_stored_file! if !cached?

            directory = File.dirname( current_path )
            tmp_path   = File.join( directory, "tmpfile" )
            File.rename current_path, tmp_path

            file = ::FFMPEG::Movie.new( tmp_path )
            #file.transcode( current_path, "-b 1500k -vcodec libx264 -vpre slow -vpre baseline -g 30 -s 640x360")
            file.transcode( current_path )

            fixed_name = File.basename(current_path, '.*') + ".mp4"
            File.rename File.join( directory, fixed_name ), current_path
            File.delete tmp_path
        end

        def make_webm
            # move upload to local cache
            cache_stored_file! if !cached?

            directory = File.dirname( current_path )
            tmp_path   = File.join( directory, "tmpfile" )
            File.rename current_path, tmp_path

            file = ::FFMPEG::Movie.new( tmp_path )
            #file.transcode( current_path, "-b 1500k -vcodec libvpx -acodec libvorbis -ab 160000 -f webm -g 30 -s 640x360")
            file.transcode( current_path )

            fixed_name = File.basename(current_path, '.*') + ".webm"
            File.rename File.join( directory, fixed_name ), current_path
            File.delete tmp_path
        end
        def make_mp3
            # move upload to local cache
            cache_stored_file! if !cached?

            directory = File.dirname( current_path )
            tmp_path   = File.join( directory, "tmpfile" )
            File.rename current_path, tmp_path

            file = ::FFMPEG::Movie.new( tmp_path )
            file.transcode( current_path )

            fixed_name = File.basename(current_path, '.*') + ".mp3"
            File.rename File.join( directory, fixed_name ), current_path
            File.delete tmp_path
        end

        private
        def prepare!
            cache_stored_file! if !cached?
        end
    end
end
