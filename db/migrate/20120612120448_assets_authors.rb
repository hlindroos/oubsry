class AssetsAuthors < ActiveRecord::Migration
    def change
        create_table :assets_authors, :id => false do |t|
            t.references :asset
            t.references :author
        end
        add_index :assets_authors, [:asset_id, :author_id], :unique => true
    end
end
