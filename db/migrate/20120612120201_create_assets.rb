class CreateAssets < ActiveRecord::Migration
    def change
        create_table :assets do |t|
            t.string :title
            t.string :description, :limit => 65535
            t.datetime :release
            t.integer :media
            t.string :file
            t.string :path
            t.integer :filesize
            t.string :checksum
            t.integer :visability
            t.integer :agelimit
            t.string :agelimitreason
            t.integer :dlcount, :default => 0

            t.string :state
            t.integer :show_id
            t.integer :category_id

            t.integer :width
            t.integer :height
            t.string :aspect
            t.string :container
            t.string :bitrate
            t.string :framerate
            t.string :samplerate
            t.integer :interlaced
            t.string :vcodec
            t.string :acodec
            t.string :channels
            t.integer :duration

            t.timestamps
        end
    end
end
