class AssetsTags < ActiveRecord::Migration
    def change
        create_table :assets_tags, :id => false do |t|
            t.references :asset
            t.references :tag
        end
        add_index :assets_tags, [:asset_id, :tag_id], :unique => true
    end
end
