class CreateShows < ActiveRecord::Migration
    def change
        create_table :shows do |t|
            t.string :name, :null => false
            t.string :description, :null => false
            t.references :asset

            t.timestamps
        end
    end
end
