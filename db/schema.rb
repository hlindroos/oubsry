# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120726073738) do

  create_table "assets", :force => true do |t|
    t.string   "title"
    t.string   "description",    :limit => 65535
    t.datetime "release"
    t.integer  "media"
    t.string   "file"
    t.string   "path"
    t.integer  "filesize"
    t.string   "checksum"
    t.integer  "visability"
    t.integer  "agelimit"
    t.string   "agelimitreason"
    t.integer  "dlcount"
    t.string   "state"
    t.integer  "show_id"
    t.integer  "category_id"
    t.integer  "width"
    t.integer  "height"
    t.string   "aspect"
    t.string   "container"
    t.string   "bitrate"
    t.string   "framerate"
    t.string   "samplerate"
    t.integer  "interlaced"
    t.string   "vcodec"
    t.string   "acodec"
    t.string   "channels"
    t.integer  "duration"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "assets_authors", :id => false, :force => true do |t|
    t.integer "asset_id"
    t.integer "author_id"
  end

  add_index "assets_authors", ["asset_id", "author_id"], :name => "index_assets_authors_on_asset_id_and_author_id", :unique => true

  create_table "assets_tags", :id => false, :force => true do |t|
    t.integer "asset_id"
    t.integer "tag_id"
  end

  add_index "assets_tags", ["asset_id", "tag_id"], :name => "index_assets_tags_on_asset_id_and_tag_id", :unique => true

  create_table "authors", :force => true do |t|
    t.string   "name",       :null => false
    t.string   "picture"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categories", :force => true do |t|
    t.string "name"
    t.string "description"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "shows", :force => true do |t|
    t.string   "name",        :null => false
    t.string   "description", :null => false
    t.integer  "asset_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tags", :force => true do |t|
    t.string "name"
  end

end
